#!/bin/bash
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

if [! command -v conda &> /dev/null]; then
    printf "${red}Conda n'est pas installe "
    printf "Pour l'installer, veuillez suivre la demarche : https://conda.io/projects/conda/en/latest/user-guide/install/index.html${reset}"
    exit
else
    echo "${red}Vous allez créer un nouvel environnement conda contenent l'ensemble des packages nécessaires à l'utilisation des scripts R${reset}"  
    read -p "${green}Entrez le nom de l'evironnement que vous souhaitez donner à cet environnement${reset} " nameEnv
    echo "${green}Creation de l'environnement : $nameEnv ${reset}" 
    conda create -n $nameEnv r-base r-tidyverse r-ggally r-reticulate r-rmysql bioconductor-made4 r-codetools r-caTools r-rprojroot r-shiny r-plotly r-knitr r-dt r-kableextra r-cluster r-gridextra r-caret r-e1071 python numpy pandas scipy scikit-learn matplotlib plotly ipykernel nb_conda_kernels
    conda install -n $nameEnv r-DBI r-RSQLite
fi


