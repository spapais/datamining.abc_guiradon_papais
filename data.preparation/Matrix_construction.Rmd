---
title: "Matrix_Construction"
author: "Sandro Papais"
date: "22/04/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
```

```{r}
library(tidyverse)
library(DBI)
library(RSQLite) # DBI package necessary
options(scipen = 999)
```

```{r}
drv <- SQLite()
db <- dbConnect(drv, dbname = "fouille_ABC")
```

```{r}
dbFetch(dbSendQuery(db, "SELECT Identification_Status, COUNT(Identification_Status) FROM TProtein GROUP BY Identification_Status"))
# On se rend compte que 17 proteines ne sont pas confirmées, on décidera de les enlever de notre analyse.
```



Sous-familles :

```{r}
dbFetch(dbSendQuery(db, "SELECT count(Subfamily) FROM TProtein WHERE Identification_Status == 'Confirmed'"))
dbFetch(dbSendQuery(db, "SELECT count(distinct Subfamily) FROM TProtein WHERE Identification_Status == 'Confirmed'"))
dbFetch(dbSendQuery(db, "SELECT count(Gene_ID), Subfamily FROM TProtein WHERE Identification_Status == 'Confirmed' GROUP BY SubFamily "))
```

Il y a en tout 15554 sous-familles dans notre tableau mais plusieurs sont répétées. Au final, il n'y a que 354 sous-familles différentes. 


Assembly :

De même que pour sous-famille on va chercher à caractériser les assemblages.
```{r}
dbFetch(dbSendQuery(db, "SELECT count(Assembly) FROM TProtein WHERE Identification_Status == 'Confirmed'"))
dbFetch(dbSendQuery(db, "SELECT count(distinct Assembly) FROM TProtein WHERE Identification_Status == 'Confirmed'"))
dbFetch(dbSendQuery(db, "SELECT count(Gene_ID), Assembly FROM TProtein WHERE Identification_Status == 'Confirmed' group by Assembly "))
```

Ici on se rend compte que certains assembly ne sont pas connus nous déciderons par la suite d'e les éliminer les protéines concernées par ces assembly vides.

Conserved Domains:

Grace a cette requete, on se rend compte que tous les CD sont bien uniques mais que les FD_ID peuvent se repeter. 
De plus on remarque que pour une proteine, il existe de multiples FD_ID associes.
```{r}
dbFetch(dbSendQuery(db, "select FD_ID, count(CD_ID) from TConserved group by FD_ID"))
dbFetch(dbSendQuery(db, "select FD_ID from TConserved WHERE Gene_ID = '8001640101005'"))
```

Avec cela, on se compte que certaines protéines n'ont pas de assembly associé. -> à supprimer de la table -> à sélectionner avant de construire la table 
-> Les mêmes tests sont a réaliser pour les autres tables. 
```{r}
dbGetQuery(db, "SELECT count(Assembly), Assembly FROM TProtein group by Assembly order by count(Assembly) desc ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Assembly == '' ")

dbGetQuery(db, "SELECT Assembly, count(Assembly) FROM TProtein group by Assembly having count(Assembly) == 1")
# Ici on se rend compte que si la n'a pas de partenaire, elle a un assembly, donc si elle n'a pas d'assembly c'est pas qu'elle est seule, c'est qu'on ne connait pas son assembly, on décidera donc d'éliminer les protéines concernées. 

dbGetQuery(db, "SELECT count(Domain_Structure), Domain_Structure FROM TProtein group by Domain_Structure order by count(Domain_Structure) desc ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Domain_Structure == '' ")
#66 pour lesquels on a un Domain_Structure vide -> à supprimer

dbGetQuery(db, "SELECT count(Subfamily), Subfamily FROM TProtein group by Subfamily order by count(Subfamily) desc ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Subfamily is null ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Subfamily == '' ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Subfamily == ' ' ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Subfamily is not null")
# Subfamily est ok; 

dbGetQuery(db, "SELECT count(Membrane_segment), Membrane_segment FROM TProtein group by Membrane_segment order by count(Membrane_segment) desc ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Membrane_segment is null ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Membrane_segment == '' ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Membrane_segment == ' ' ")
dbGetQuery(db, "SELECT count(Gene_ID) FROM TProtein where Membrane_segment is not null")
#Membrane segment est ok
```

Lecture de la table protéine, à laquelle on sélectionne les proteines pour lesquelles on a un status confimé d'ABC et dont le type est ABC, le assembly et le domaine structure n'est pas vide (block précédent).
```{r}
df = dbFetch(dbSendQuery(db, "SELECT Gene_ID, Chromosome, Integrity, Subfamily, Assembly, Domain_Structure, Membrane_segment FROM TProtein WHERE Identification_Status = 'Confirmed' AND Type = 'ABC' AND Domain_Structure != '' AND Assembly != ''"))
```

Ajout de la fonction dans une colonne, sélection des gènes qui sont dans notre table
```{r}
vector_Function <- c()
for (val in df$Gene_ID) {
  result <- dbFetch(dbSendQuery(db, "SELECT Function FROM TGene WHERE Gene_ID = :variable", params = list(variable = val)))[1,1]
  vector_Function <- c(vector_Function, result)
  
}

df <- cbind(df, Function = vector_Function)
```


Ajout des FD domains pour lesquels on a une e_value < 0.05.
```{r}
'%notin%' <- Negate('%in%')

empty_vector <-rep("Absent",nrow(df)) # Vecteur d'"absent" qui sera ajouté pour chaque FD
for (gene in df$Gene_ID) {
  vector_FD_ID <- c() # Pour un gene il existe plusieurs FD -> permettra de les stocker
  result <- dbFetch(dbSendQuery(db, "SELECT FD_ID FROM TConserved WHERE e_value < 0.05 AND Gene_ID = :variable", params = list(variable = gene)))[1,1] # récupère le FD pour un Gene, plusieurs fois et ajoute dans le vecteur 
  vector_FD_ID <- c(vector_FD_ID, result)
  for (fd in vector_FD_ID){  # Pour tout ce qu'on a dans le vecteur ( tous les FD associés a un gene)
    if( fd %notin% colnames(df) && !(is.na(fd)) ){
      df[[fd]] <- empty_vector
      df[which(df[['Gene_ID']] == gene),grep(fd, colnames(df))] <- "Present"
    }else if (!(is.na(fd))){
      df[which(df[['Gene_ID']] == gene),grep(fd, colnames(df))] <- "Present"
    }
    
  }
  
}
```


Ecriture de la table obtenue dans un fichier tsv.
```{r}
write.table(df, file = "matrice_FD.tsv", sep = "\t", row.names = F, col.names = T )
```

Quelques testes utiles pour l'analyse knime :
```{r}
dbGetQuery(db, "SELECT count(distinct Gene_ID), count(Gene_ID) FROM TABC")
dbGetQuery(db, "SELECT count(distinct Subfamily), count(Subfamily) FROM TABC")
dbGetQuery(db, "SELECT count(distinct Assembly), count(Assembly) FROM TABC")
dbGetQuery(db, "SELECT count(distinct Domain_Structure), count(Domain_Structure) FROM TABC")
```


Réouverture si besoin de la table.
```{r}
df = read.table("matrice_FD.tsv", sep = "\t", head = T)
```



```{r}
dbDisconnect(db)
```
