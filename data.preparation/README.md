Scripts accompagnés de la documentation utilisateur permettant d'obtenir la matrice *individus-variables* qui servira pour le projet.

# A l'attention de l'utilisateur 

Ce document décrit comment utiliser les différents fichiers R afin d'obtenir une matrice individus-variables permettant la classification de gènes codant pour des partenaires ABC selon leur architecture en domaine. 

## Prérequis 

### Données 

Pour ce projet, les tables suivantes seront nécessaires :

- Protein.tsv
- Gene.tsv
- Conserved_Domain.tsv
- Functional_Domain.tsv
- Assembly.tsv

Attention, environ 5,3Go sont nécessaires pour le stockage de l'ensemble des fichiers, base de données, environnement conda ( 2,3Go pour la base de données, 1,8Go pour l'environnement conda ).

### Chargement des tables dans une base de données SQLite

Un fichier installation.bash est fourni afin de créer l'environnement conda contenant les diverses librairies R nécessaires au chargement des tables dans une base de données SQLite. Il devra être exécuté à partir d'un terminal avec la commande :

    ./installation.bash 
	
Les instructions seront à suivre depuis ce terminal. Cet environnement conda devra être chargé lors de l'utilisation des scripts R à l'aide de la commande : 

    conda deactivate && conda actiavte /Nom de votre environnement/
  
Un fichier de désinstallation est aussi disponible afin de désinstaller cet environnement ultérieurement ( voir fin de ce présent document ). 

Le package RSQLite chargé dans l'environnement conda permet l'utilisation des fichiers R ( database_construction.R et Matrix_construction.R ) et ainsi charger les tables dans une base de données locale SQLite puis d'utiliser cette base de données afin de construire la matrice. Ce systeme, permet une simplification dans la construction de la matrice finale individus-variables notamment par l'utilisation de requêtes SQL directement depuis le logiciel R. 

Le fichier Database_construction.R doit donc être lancé pour la construction de la base de données avant l'utilisation du script Matrix_construction.R;
Celui ci contient le chargement des librairies nécessaires. Les tables nécessaire à notre étude sont chargées en tant que dataframes ( read.table ) . Le choix des tables sera détaillé par la suite. La connexion à la base de données fouille_ABC et établie puis les tables sont créées dans cette base de données ( déclaration des tables et de leurs attributs, écriture des dataframes chargées préalablement puis fermeture de la connexion ).

## Script de construction de la matrice Matrix_construction_script.R

La base de données étant créée et opérationnelle, nous pouvons maintenant utiliser des requêtes SQL afin de récupérer les données qui nous intéressent dans la base de données. Il aurait aussi pu être intéressant de charger toutes les tables dans la base pour de futures recherches mais nous n'en avons pas besoin pour le moment.

Le script Matrix_construction_script.R permet donc la construction de la matrice à proprement parler.  
Le repertoire de travail est changé en début de script pour être celui où se trouve le fichier.  
Les librairies tidyverse ( pour la manipulation de dataframe ), DBI et RSQLite ( pour l'utilisation de requêtes SQL ) sont chargées.  
L'option scipen = 999 permet de s'affranchir de l'annotation scientifique notamment pour les Gene_ID qui seront transformés sans cette option.  

### Connexion à la base de données et chargement de la table

Premièrement la connexion à la base de données locale "fouille_ABC" est établie. 

Puis un premier dataframe df est construit grâce à la requête SQL dans la base de données locale "fouille_ABC" préalablement créée.

    df = dbFetch(dbSendQuery(db, "SELECT Gene_ID, Chromosome, Integrity, Subfamily, Assembly, Domain_Structure, Membrane_segment FROM TProtein WHERE Identification_Status = 'Confirmed' AND Type = 'ABC' AND Domain_Structure != '' AND Assembly != ''"))
  
Grâce à cette requête, on sélectionne dans la table TProtein ( correspondant au fichier Protein.tsv ) le colonnes Gene_ID, Chromosome, Integrity, Subfamily, Assembly, Domain_Structure, Membrane_Segment. Une condition est donnée sur Identification_Status ( status d'identification ) qui doit être confirmé ( le gène est bien un partenaire ABC ) ainsi que sur Type afin de confirmer que nous sélectionnons seulement les Genes codant pour des partenaires ABC puis sur DOmain_Structure et Assembly qui pour certains Gene_ID renvoient des ensembles vides. 

### Récupération de la fonction des Genes

Un vecteur vide vector_Function est crée. Ce vecteur se complete à chaque iteration d'une boucle for sur les Gene_ID du dataframe et permettra de stocker l'ensemble des fonctions se trouvant dans la table TGene ( fichier Gene.tsv ). Enfin une jointure est réalisée entre le vecteur et le dataframe.
( Il s'est avéré par la suite que cet attribut fonction ne serait finalement pas utilisé dans notre classification ).

### Ajout d'un ensemble de colonnes correspondant aux différents FD_ID

Etant donné que plusieurs FD_ID peuvent être associés à un seul Gene, ainsi que les FD_ID peuvent être associés à plusieurs gènes, il n'est pas possible d'utiliser l'un ou l'autre comme individu dans notre dataframe. Une solution à ce problème est de créer un attribut pour chaque FD_ID et celui-ci sera codé comme 'Present' ou 'Absent' en face de chaque Gene_ID.  
Pour ce faire, deux boucles imbriquées ont été nécessaires ainsi qu'un vecteur de la taille ( nombre de lignes ) du dataframe contenant seulement la valeur "Absent" est crée. Il permettra d'initialiser chaque nouveau FD_ID vu, qui sera un nouvel attribut, à absent, ce pour chaque Gene_ID passé dans la boucle. La première boucle sur les Gene_ID de notre dataframe permet d'insérer dans un vecteur vide vector_FD_ID, le résultat de la requête result nous permettant d'obtenir pour un Gene_ID, tous les FD_ID de la table TConserved ( fichier Conserved_Domains.tsv ) pour lesquels la e_value est supérieure à 0.05.  
Dans la seconde boucle, tous ces FD_ID correspondant à un gène, sont ajoutés en temps que colonne dans le datafrarame ( colonne initialisée à "Absent" partout grâce au vecteur empty_vector créé ) et les Gene_ID concernés sont passés à "Present", seulement si la colonne du FD_ID n'est pas déjà présente dans le dataframe. Si celle si est déjà présente, elle n'est pas ajoutée mais le FD_ID correspondant au Gene_ID sera quand même passé à "Present".

Pour résumer, nous avons à présent une matrice contenant comme individu tous les Gene_ID récupérés dans la table Proteins.tsv, étant confirmés comme faisant parti d'un système ABC.  
Pour chacun de ces gènes, nous avons le chromosome qui le porte, son intégrité, sa sous-famille, son numero d'assemblage, la structure en domaine de la protéine, le nombre de segments transmembranaires, la fonction de sa proteine.  
Enfin nous avons aussi pour tous les gènes, la liste de FD_ID qui sont présents au moins une fois dans notre liste de gènes. Chaque cellule porte la valeur Presence si le gène possède le domaine, absence s'il ne le possède pas. 

Cette partie est plutôt longue à tourner et peut prendre jusqu'à 1 heure pour tourner entièrement. Une simplification de notre algorithme pourrait être apportée, notament dans l'imbrication des boucle qui augmentent la complexité de notre algorithme. 

Une fois la matrice obtenue, celle ci est exportée ( matrice_FD.tsv ) et se trouvera dans le dossier data.preparation; 

## Classification

Au final cette matrice nous permettra de construire un modèle capable de classifier de nouveaux gènes selon leur structure en domaine protéique dont on sait qu'ils font parti d'un système ABC, en fonction des attributs représentant les différents domaines fonctionnels, le nombre de segments transmembranaires, le chomosome, et l'intégrité. Nous avons choisi de charger dans cette matrice les domaines fonctionnels qui représentent pour nous la plus grande source d'information afin de classer les genes selon leur structure en domaine protéique. Ainsi les différents attributs restant permettront peut être d'affiner cette classification.  
De plus, ces attributs restant pourraient nous servir pour de futures classifications notamment déterminer la sous-famille ainsi que de déterminer les partenaires d'assemblage du gène. Dans cette optique de futures modifications seraient à apporter à notre matrice. 


Après l'obtention de la matrice, vous pouvez désinstaller l'environnement conda grace au fichier Uninstallation.bash; Attention, après cela, il ne sera pas possible d'effectuer des requêtes SQL. L'execution de se fichier se fait à l'aide de la commande :

    ./Uninstallation.bash
