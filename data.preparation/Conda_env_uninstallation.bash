#!/bin/bash
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo "${green}Voici la liste de vos environnements : ${reset}" 
conda env list
read -p "${green}Entrez le nom de l'environnement à supprimer ${reset} " nameEnv
conda env remove --name $nameEnv
