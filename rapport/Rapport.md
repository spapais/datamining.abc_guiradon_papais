# Projet Fouille de données

# Identification et Classification de systèmes ABC

*Papais Sandro et Antoine Girardon, M1 Bioinformatique et Biologie des Systèmes*

# Contexte

Les transporteurs ABC ( ATP-biding cassette transporter ) représentent une large famille multi-génique de transporteurs. Elle s'étend sur l'ensemble des taxons, des procaryotes aux eucaryotes, en passant par les archées.  
Ces transporteurs sont la plupart du temps composés d'une sous-unité transmembranaire, assurant le passage à travers la membrane plasmique et d'une sous-unité associée à la première. Cette seconde sous-unité effectue l'hydrolyse de l'ATP, libérant l'énergie nécessaire au transport des molécules à travers le pore formé par la première.  
Cette organisation est très conservée au sein des 3 règnes du vivant avec des variations retrouvées selon le sens du transport : import ou export. De manière générale, on retrouve 2 domaines associés pour les importeurs et 3 pour les exporteurs.

![](transporteurs.png)

**Architecture des transporteurs ABC** MSD : Membrane Spanning Domain, NBD : Nucleotide Binding Domain, SBP : Solute Binding Protein

Comme on peut le voir sur ce schéma, parmi ces domaines, on retrouve :

- **MSD** : Membrane Spanning Domain. L'association de 2 domaines MSD permettent la formation d'un pore à travers la membrane plasmique

- **NBD** : Nucleotide Biding Domain. L'association de 2 domaines NBD fournit l'énergie nécessaire au transport de la molécule à travers le pore formé.

- **SBP** : Solute Binding Protein. Un domaine unique capture par liaison le substrat pour le guider vers l'entrée du port.

L'association de ces différents domaines ( 1 à 5 selon la structure des gènes codant ces protéines ) permet la formation du transporteur.

Etant donné le contexte actuel, de plus en plus de données sont collectées. Mais ces collections de données ne représentent rien sans avoir été annotées. C'est pourquoi dans le cadre du projet de recherche en fouille de données, nous allons nous intéresser à l'utilisation des données actuellement présentes sur les bases de données afin de construire un classificateur permettant l'annotation de nouveaux gènes codant pour des protéines impliquées dans les systèmes ABC.

Plusieurs axes de prédictions s’offrent à nous :  

- prédiction si un gène code ou pas pour un partenaire d’un système ABC  
- si c’est bien le cas :  
- déterminer son architecture en domaines, par exemple NBD-MSD
déterminer la sous-famille à laquelle il appartient  
- de déterminer quels sont ses partenaires, c’est-à-dire quels gènes codent pour des protéines qui vont in vivo s’assembler pour former le système


La première étape représentera la sélection des données intéressantes à notre classificateur, ainsi que le choix de la prédiction permettant une future annotation que nous souhaitons réaliser.  
Dans une deuxième partie, nous décrirons l'approche retenue afin de créer un classificateur cohérent avec les données choisies.  
Dans une troisième partie, nous décrirons la méthode qui nous a permis d'obtenir une matrice contenant les informations nécessaires à la construction du classificateur ainsi que la construction à proprement parler du classificateur.  
Enfin nous analyserons les résultats obtenus par notre classificateur ainsi que sa précision et nous discuterons des résultats obtenus.


# Analyse

### Objectifs à atteindre

En Data mining, l'objectif principal est l’extraction d’informations intéressantes via la manipulation données conséquentes afin d'y extraire des informations intéressantes : des patterns.  

Plusieurs méthodes s'offrent à nous en Data mining. Dans notre étude, nous utiliserons la classification. Le but de la classification consiste à caractériser des classes selon leurs attributs afin de pouvoir construire un modèle capable de prédire la classe d’objets à partir de la valeur de leurs attributs : nos données.
Pour cela nous avons besoin d'un jeu d'apprentissage, où chaque objet appartient à une classe déjà identifiée. C'est le modèle classificateur, qui nécessite différents algorithmes comme, les arbres de décision, les forêts aléatoires, la classification bayésienne naïve, etc. Il sera nécessaire par la suite d’évaluer les performances de notre modèle à l'aide d'un jeu de test. En obtenant la part d'objets correctement associée à sa classe, on peut en déduire la performance du classifieur. Si notre performance est significative on peut donc utiliser ce modèle pour classer d'autres données à partir des classes obtenues par le jeu d'apprentissage.  

![](schema_classification.png)

**Schéma des étapes lors d'une classification en data_mining** - Conférence fouille de données_Roland Barriot

Nous devons donc réalisé ces différentes étapes :  

- Construction de la matrice individus-variable. Cette étape est à réaliser après avoir appréhendé les données. Il s’agit de choisir un certain nombre d’attributs cohérents qui permettent de classer les différents gènes selon les classes qui nous intéressent. Cette étape est cruciale dans notre analyse car la matrice étant longue à construire, nous préférons ne pas manquer d’y ajouter un attribut qui pourrait être important, celui-ci pourra par la suite être facilement supprimé de nos données s'il s’avère non concluant.  

- Analyse de la matrice grâce au logiciel KNIME. Le logiciel KNIME possède toutes les fonctionnalités requises au partitionnement aléatoire des données en un jeu de test et un jeu d’apprentissage, la construction du modèle de classification à partir du jeu d’apprentissage, utilisation du modèle sur le jeu de test puis évaluation des performances du modèle sur la base de la comparaison des classes connues du jeu de tests et les classes qui ont été prédite sur ce même jeu grâce à notre modèle.  

- Le modèle sera alors destiné à classer des objets pour lesquelsnous possédons les attributs mais pas la classe. Cette étape ne sera pas présente dans notre analyse.  

### Analyse préliminaire des données  

Les données fournies sont extraites de la banque de données publique ABCdb https://www-abcdb.biotoul.fr/. De cette base de données ont été extraites et mises à disposition pour notre étude, 9 tables :

- les génomes complets (tables Strain et Chromosome) et leur classification taxonomique (Taxonomy)  
- les gènes de chaque génome (table Gene)  
- les domaines identifiés sur les gènes (tables Conserved_Domain indiquant leur position et - leur score, et Functional_Domain)  
- les relations d’orthologie 1:1 (table Orthology)  
- les gènes identifiés comme ABC (table Protein) et les systèmes réassemblés (table Assembly)  

Nous décidons que la classification de l’architecture des domaines sur les protéines serait une première approche intéressante à développer pour une future prédiction sur des protéines dont on ne connaît pas leur architecture. En effet, on remarque que nous possédons au total 15537 gènes différents étant confirmés pour être codant pour des protéines faisant partie de partenaires ABC. À partir de ces protéines nous serions donc capables d’en extraire parmi leurs attributs, des patterns intéressants à notre prédiction. Il ne faut pas oublier de rester cohérent dans le choix de nos attributs. En effet, il n’est pas possible d'utiliser les systèmes assemblés de la table assembly pour en prédire l’architecture en domaine d’une protéine car cette table constitue une étape ultérieure à notre prédiction. Lors de l'utilisation de ce modèle, nous ne serions donc pas en mesure de donner cet attribut à notre modèle. 

Cette analyse s'inscrit dans le but de pouvoir s'intéresser par la suite aux différents partenaires que peuvent constituer un système ABC, axe de recherche qui ne sera pas abordé dans notre présente analyse. Pour cela, certains attributs seront relevés pour de futures recherches. 

Le choix des tables et des attributs est donc très important car il constitue le fondement de notre analyse. De plus, un nettoyage des données pourrait s’avérer nécessaire.  
Nous décidons donc de nous intéresser aux attributs suivants :  

- Gene_ID qui sera en réalité l'identifiant de l’individu  
- Chromosome ( les partenaires pourraient être plus fréquent si leurs gènes sont présents sur un même chromosome ).  
- Subfamily : attribut qui pourrait aider à déterminer l’architecture des domaines sur la protéine   
- Assembly : attribut qui pourrait aider à déterminer quels sont les partenaires dans un système ABC )  
- Domain_Structure : la classe que nous souhaitons caractériser  
- Membrane_segment : attribut qui pourrait aider à déterminer l’architecture des domaines sur la protéine  
- FD_ID : Chaque FD_ID présent sur au moins une de nos protéines constituera un attribut. Celui-ci sera codé “Absent” ou "Présent" selon s’il est retrouvé dans la protéine. 

Nous avons donc récupéré les tables suivantes dans une base de données, avec leurs relations : 


![Base de données](image_database.png)

De cette base de données, seront extraits les individus, attributs et classe décrits plus haut. 

- Une sélection des Gene_ID dans la table Protein sera effectuée sur leur type ‘ABC’ et leur Identification_Status ‘Confirmed’.   
- Nous éliminons les Gene_ID pour lesquels Domain_Structure et Assembly son vides.  
- Enfin, les FD_ID sélectionnés, sont ceux ayant une e_value inférieure à 0,05 ( deux tables supplémentaires evalue < 0,01 et 0,001 ).   

Ainsi, nous nous assurons que les données sur lesquelles nous travaillons, sont des données vérifiées, permettant ainsi une augmentation dans nos chances d’obtenir un modèle précis. 

# Conception

### Construction de la matrice sous le logiciel R

La construction de la matrice est détaillée dans le document README.md du dossier data.preparation.  
La matrice obtenue sera utilisée dans le logiciel KNIME afin de construire et tester notre modèle de classification.  

### Analyse sous le logiciel KNIME

Le logiciel KNIME est un outil gratuit permettant l’analyse de données, et notamment la possibilité de construire des modèles de prédiction. Plusieurs méthodes de classifications sont possibles. Parmi lesquelles on retrouve les arbres de décision, les forêts aléatoires, les classificateurs bayésiens naïfs. Nous allons donc tester ces modèles, avec différents paramètres afin d’en tirer le meilleur modèle possible.

Un workflow a été créé pour ce projet. Il contient un ensemble de nœuds qui représentent les étapes de la classification. Chaque nœud est orienté et relié afin de réaliser les différentes étapes dans l’ordre.

 **Chargement des données** 

Le chargement des données s’effectue à l’aide d’un ‘File Reader’. La configuration du File Reader doit contenir les paramètres suivants : le document à charger qui correspond à la matrice créée, sélectionner Row_ID puis <tab> dans column delimiter. Certaines colonnes peuvent être ignorées dans l’analyse (clic sur une colonne>Don’t include wolumn in output table).

La matrice peut mettre un peu de temps à charger. Une fois les paramètres sélectionnés, valider. 

**Partitionnement des données**

La sortie du File Reader est raccordée à l’entrée du X-Partitionner qui permet le partitionnement des données en un jeu d’apprentissage et un jeu de test. ( ⅔ - ⅓ ).  
Les paramètres à choisir dans le partitionneur sont le nombre de validations qui correspond au k lors du test de validation croisées. Puis nous pouvons choisir le type de partitionnement : linéaire, aléatoire, stratifié.

Le partitionneur propose 2 sorties. Une qui sera reliée au bloc d’apprentissage du modèle : *Nom_du_Modèle* Learner ( Training Data ) et l’autre sera reliée au bloc Predictor du modèle ( Test Data ).

**Construction du modèle**

Le bloc *Nom_du_Modèle* Learner permet la construction du modèle. Selon la méthode utilisée, les paramètres diffèrent.  
Pour les arbres de décisions, il faudra préciser la classe à prédire ( Class column ), la méthode de calcul pour la sélection des attributs qui permettront de construire l’arbre ( Quality measure ), et enfin le type d’élagage ( Pruning method ).  
Pour le classificateur Bayésien Naïf, il faudra préciser la classe à prédire ( Classification Column ), la probabilité utilisée par défaut ( la probabilité qui sera assignée lorsque l’observation est en fréquence 0, Default probability).  
La sortie du Learner sera reliée à l'entrée du *Nom_du_Modèle* Predictor.

**Utilisation du modèle sur le jeu de test** 

Le bloc *Nom_du_Modèle* Predictor permet de réaliser les prédictions sur le jeu de test grâce au modèle construit précédemment. C'est ce bloc qui utilise le modèle sur notre jeu de test afin de prédire les classes sur les objets de ce jeu. Ce bloc permettra l'obtention de la Prediction column utilisé dans le X-Aggregator. On y connecte donc les sorties du *Nom_du_Modèle* Learner et X-Partitioner.  
Sa sortie sera redirigée vers l'entrée d’un X-Agregator.

**Comparaison des résultats** 

Le X-Aggregator permet de comparer les classe prédites obtenus avec notre *Nom_du_Modèle* Predictor aux classes réelles de notre jeu de test. Il est important d’y préciser dans les paramètres, la classe réelle de nos objets ( Target column ), et la classe prédite par le Predictor ( Prediction column ).
Sa sortie sera redirigée vers un bloc Statistics.

**Analyse des résultats** 

Le bloc Statistics permet de calculer les résultats notamment le taux d’erreurs. Ce taux d’erreurs correspond donc au taux d’erreur de prédictions du Prédictor, en utilisant le modèle précédemment construit, sur le jeu de test, en comparaison avec les classes réelles du jeu de test.


Une fois le workflow entièrement créé et connecté, l’exécution sera lancée, soit bloc par bloc à l’aide de la touche F7, soit en exécutant le dernier bloc qui lancera l’exécution de tous les blocs précédents. Voici les workflow obtenu dans notre cas, c’est à dire l’utilisation d’arbre de décision et de classificateur Bayésien naïf en temps que méthodes de classification :


![Workflow KNIME(Knime_workflow.png)

**Exemple de workflow obtenu avec le logiciel KNIME**

# Réalisation

### Les paramètres testés

Lors de notre analyse, plusieurs paramètres seront testés. De même nous testerons plusieurs méthodes de classification. Ceci dans le but de tenter d’obtenir le meilleur classificateur possible, c'est-à-dire le classificateur produisant le taux d’erreur le plus faible possible.

Pour la plupart des résultats d'analyses présenté, les données utilisées sont celles contenues dans notre matrice matrice_FD.tsv. D'autres matrices ont été testées et les résultats seront aussi présentés. Plusieurs combinaisons d’attributs ont été testées. Comme mentionné plus haut, les colonne Assembly et Fonction ne sont pas inclues dans l’analyse. Assembly pourrait être utilisée dans de futures analyses de prédiction de partenaires ABC. L'attribut Fonction quant-ç lui possède trop de modalités différentes pour être utilisé. 

Nous choisissons pour le X-Partitioner 2 valeurs pour le nombre de validations lors du test de validations croisées : 3 et 10.  
Nous testerons premièrement un partitionnement aléatoire qui semble plus pertinent qu’un partitionnement linéaire ainsi qu’un partitionnement stratifié sur la colonne Domain_Structure qui correspond à nos différentes classes. 

Concernant les différentes méthodes de classification, nous avons choisi d’utiliser les arbres de décisions ainsi que le classificateur Bayésien naïf.  
Pour les arbres de décisions, nous testerons différents paramètres de mesure : ratio de Gain ou ratio Gini, sans élagage ou avec un élagage MDL.  
Pour le classificateur Bayésien naïf, nous testerons différentes valeurs de Default probability.
    
### Résultats et discussion

![](knime_results.png)
**Résultats obtenues avec différentes méthodes et paramètres**  

Concernant les modèles obtenus par la méthode des arbres de décision, nous traiterons principalement les mesures obtenues par le partitionnement aléatoire qui a permis l'obtention de meilleurs résultats que le partitionnement stratifié. Les données obtenues avec un partitionnement stratifié sont tout de même disponibles dans la table des résultats.  
Nous n’avons pas choisi le partitionnement linéaire car celui si a forcément plus de chances d’obtenir des résultats biaisés qu’avec un partitionnement aléatoire.

**Sur les modèles obtenus grâce aux arbres de décision :**  

D’après nos résultats, 10 validations permettent d’obtenir un classificateur plus précis. En effet, nous obtenons un taux d’erreur en moyenne plus faible avec 10 validations plutôt que 3 validations ( 6,86% et 6,72% d’erreur en moyenne respectivement ).

Concernant le paramètre Quality Mesure ( qui correspond à la méthode de calcul des attributs à choisir pour la construction de l’arbre de décision ), il semblerait que Gini soit en moyenne plus précis que Gain ( 6,81% et 6,77% d’erreurs en moyenne respectivement ).

Pour les paramètres d’élagage, nous obtenons de meilleurs résultats lorsque les arbres ne sont pas élagués plutôt qu’après élagage MDL ( 6,52% et 7,07% d’erreur respectivement ) 

On pourrait donc s’attendre à ce que les meilleures paramètres soient : 

* 10 validations croisées;
* Gini;
* no pruning;

Mais ce n’est pas le cas. En effet, on remarque que le modèle obtenant le plus faible taux d’erreur ( 6,4364% ) et qui donc serait le meilleur classificateur, le plus précis, est celui dont les paramètres sont : 

* 10 validations croisées;
* Gain;
* no pruning;

Il semble donc qu’il existe un lien entre les différents paramètres, c’est-à-dire que la présence de l’un influerait sur les résultats de l’autre paramètre. Mais cette différence n’a pas été vérifiée statistiquement. 

De plus, on peut remarquer que nous obtenons de meilleurs résultats avec le paramètre ‘no pruning’, ce qui est contre-intuitif. En effet, l’élagage est censé affiner l’arbre, qui peut dans certaines situations trop refléter le jeu de données et ainsi être moins adapté à des nouvelles données. Il semblerait donc que dans notre cas, cet élagage ai supprimé des branches utiles à notre modèle. 

Après avoir testé différentes combinaisons d’attributs, il apparaît que la suppression des attributs Chromosome, Integrity et/ou Subfamily ne semble pas ou très peu influer sur le taux d’erreurs. Seule la suppression de l’attribut Membrane_segment dans le File Reader ( nombre de segments transmembranaires ) ainsi que de la totalité des FD_ID ( par la création d’une seconde matrice sans FD_ID : matrice_no_FD.tsv ) voyait une augmentation du taux d’erreur à 12% et 32% respectivement. Nous en concluons donc que ces 2 attributs sont les plus importants de notre analyse.

2 autres matrices ont été crées dans le but de cette fois-ci sélectionner les FD_ID pour lesquels la e_value était inférieure à 0,01% et 0,001% ( matrice_FD_01.tsv et matrice_FD_001.tsv). Celles-ci ont permis la construction de modèles sur la base des paramètres avec lesquels nous avions obtenu les meilleurs résultats ( 10 validations, Gain, nopruning ) . Avec ces deux matrices, le taux d'erreurs est sensiblement plus faible : 6,208% et 6,221% d'erreurs respectivement.  


**Sur les modèles obtenus grâce aux classificateurs Bayésiens naïfs :**

Le paramètre qui nous intéressera ici est ‘Default probability’. Nous nous sommes basés premièrement sur une prababilité par défaut de 0,000064. Cette probabilité représente la plus faible fréquence que nous pouvions obtenir pour un domaine fonctionnel ( 1/15534 ). Puis nous avons testé 0,000032 ( /2 ) et 0,0001 ( ~ x2).   Ces 3 valeurs ont été testées pour 3 ou 10 validations.
Dans les 6 situations, aucun des modèles construits n'a permis d'obtenir un taux d'erreur inférieur à 20%, avec un avantage pour les modèles construits avec 3 validations ( 21,13% d'erreurs en moyenne pour 3 validations contre 29,36% d'erreurs en moyenne pour 10 validations ).


On conclut donc qu'avec notre matrice, l’approche de l’arbre de décision semble bien meilleure que celle du bayesien naïf. En effet cette dernière repose sur une hypothèse d’indépendance des attributs, ici on sait que la présence de domaine pourrait être corrélé avec d’autre domaine sur la protéine.  
Toutefois, les résultats obtenus avec l'arbre de décision ne permettent pas d'obtenir un classificateur statistiquement significatif ( taux d'erreur inférieur à 5% ).  

# Bilan et perspectives

Nous sommes globalament satisfait de ce travail bien que nous espérions au moins obtenir un classificateur permettant l'obtention d'un taux d'erreurs inférieur à 5%. 
Concernant la construction de la matrice, notre première approche a été l’utilisation des pandas dans python afin de pouvoir manipuler les tables données fournies. Étant novices dans l’utilisation de Pandas, il s’est avéré très difficile de se former à l’utilisation de cet outil sur des jeux de données pour lesquels il fallait en même temps comprendre chaque variable et pouvoir être capable de sélectionner les données souhaitées.  
De plus, l’utilisation d’une base de données représente un réel avantage lors de la sélection des Gene_ID ( élimination de ceux pour lesquels Assembly était vide, sélection des FD_ID avec une e_value < 0,05% … ).  
C’est pourquoi nous nous sommes finalement tournés vers R, logiciel sur lequel nous avions plus de connaissances et sur lequel il était très simple de construire une base de données et d’y réaliser des requêtes ( bien que l’utilisation de bases de données reste possible sur python ). 

L’utilisation d’une base de données a été très formateur car nous n’avions auparavant jamais utilisé RSQLite. De plus cela nous a permis de revoir la construction formulation de requêtes SQL.

Nous avions premièrement prévu de réaliser tous les objectifs, de la prédiction du type ( ABC ou non ) jusqu’à la prédiction des partenaires ABC. Mais nous nous sommes rendu compte que ce travail était beaucoup trop lourd, demandait plus de temps mais aussi la construction d’une matrice contenant beaucoup plus d’attributs permettant les différentes prédictions. Nous avons donc dû revoir nos objectifs à la baisse.


# Gestion du projet

Le projet a été mené par l’intermédiaire d’un répertoire partagé Git permettant une simplification du partage des différents fichiers au sein du groupe. Il a ainsi permis que chacun soient au fait de chaque modification au sein des différents codes(r-matrice) / workflows(knime-analyse).
    
Nous nous sommes premièrement mis d’accord pour décider quels seraient nos objectifs ainsi que les méthodes dont nous aurions besoin afin de pouvoir répondre à ces objectifs ( objectifs qui ont finalement changé au cours de notre travail ).

Sandro s’est plus impliqué dans la conception et construction de la matrice individus-variables, notamment la compréhension de RSQLite, la création de la base de données et la formulation des différentes requêtes, ainsi que le remplissage de la matrice. 

Antoine s’est plus impliqué dans la construction des différents workflow sur KNIME,  l’analyse des données et la récolte des différentes données obtenues pour les différents paramètres/méthodes testés. 

La rédaction du rapport a été plutôt partagée et réalisée conjointement. Nous avons trouvé judicieux que chacun participe à la rédaction de la partie sur laquelle il s’était moins investis. Cela nous a permis de nous assurer que tous les points sur lesquels nous avions moins travaillés étaient compris et maîtrisés. 

Un de nos regrets est que nous aurions du utiliser un diagramme de Gantt dès le début pour nous organiser. Etant donné que nous avions plusieurs travaux de groupes, il était difficile de respecter les délais que nous nous fixions, chacun ayant des motivations différents à des instants différents. Mais nous nous en sommes aperçu trop tard donc avons décidé d'abandoner ce projet. 



